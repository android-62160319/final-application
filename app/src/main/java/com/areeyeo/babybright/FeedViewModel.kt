package com.areeyeo.babybright

import androidx.lifecycle.*
import com.areeyeo.babybright.data.Feed
import com.areeyeo.babybright.data.FeedDao
import kotlinx.coroutines.launch

class FeedViewModel(private val feedDao: FeedDao) : ViewModel() {

    val allItems: LiveData<List<Feed>> = feedDao.getItems().asLiveData()

    private fun insertFeed(feed: Feed) {
        viewModelScope.launch {
            feedDao.insert(feed)
        }
    }

    private fun getNewFeedEntry(datetime: String, food: String, reaction: String): Feed {
        return Feed(
            datetime = datetime,
            food = food,
            reaction = reaction
        )
    }

    fun addNewFeed(datetime: String, food: String, reaction: String) {
        val newFeed = getNewFeedEntry(datetime, food, reaction)
        insertFeed(newFeed)
    }

    fun isEntryValid(feedDateTime: String, feedFood: String, feedReaction: String): Boolean {
        if (feedDateTime.isBlank() || feedFood.isBlank() || feedReaction.isBlank()) {
            return false
        }
        return true
    }

    fun deleteFeed(feed: Feed) {
        viewModelScope.launch {
            feedDao.delete(feed)
        }
    }

    fun retrieveItem(id: Int): LiveData<Feed> {
        return feedDao.getItem(id).asLiveData()
    }
}

class FeedViewModelFactory(private val feedDao: FeedDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FeedViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FeedViewModel(feedDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
