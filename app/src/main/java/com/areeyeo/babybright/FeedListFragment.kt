package com.areeyeo.babybright

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.areeyeo.babybright.adapter.FeedListAdapter
import com.areeyeo.babybright.databinding.FragmentFeedListBinding
import com.areeyeo.babybright.databinding.FragmentMenuBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FeedListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FeedListFragment : Fragment() {
    private val viewModel: FeedViewModel by activityViewModels {
        FeedViewModelFactory(
            (activity?.application as FeedApplication).database.feedDao()
        )
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentFeedListBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFeedListBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         val adapter = FeedListAdapter {
            val action =
                FeedListFragmentDirections.actionFeedListFragmentToFeedDetailFragment2(it.feed_id)
            this.findNavController().navigate(action)
        }
        binding?.recyclerViewFeed?.adapter = adapter
        binding?.btnAdd?.setOnClickListener {
            val action = FeedListFragmentDirections.actionFeedListFragmentToAddFeedFragment()
            view.findNavController().navigate(action)
        }
        binding?.recyclerViewFeed?.layoutManager = LinearLayoutManager(this.context)

        // Attach an observer on the allItems list to update the UI automatically when the data
        // changes.
        viewModel.allItems.observe(this.viewLifecycleOwner) { items ->
            items.let {
                adapter.submitList(it)
            }
        }

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}