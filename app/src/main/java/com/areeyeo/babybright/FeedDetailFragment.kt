package com.areeyeo.babybright

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.areeyeo.babybright.data.Feed
import com.areeyeo.babybright.databinding.FragmentFeedDetailBinding
import com.areeyeo.babybright.databinding.FragmentFeedListBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FeedDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FeedDetailFragment : Fragment() {
    private val navigationArgs: FeedDetailFragmentArgs by navArgs()
    lateinit var feed: Feed

    private var _binding: FragmentFeedDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FeedViewModel by activityViewModels {
        FeedViewModelFactory(
            (activity?.application as FeedApplication).database.feedDao()
        )
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFeedDetailBinding.inflate(inflater, container, false)
        return binding.root
         }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.retrieveItem(id).observe(this.viewLifecycleOwner)
        {selectedItem ->
            feed = selectedItem
            bind(feed)
        }

    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteFeed()
            }
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun deleteFeed() {
        viewModel.deleteFeed(feed)
        findNavController().navigateUp()
    }

    private fun bind(feed: Feed) {
        binding.apply {
            datetimeDetail.text = feed.datetime
            foodDetail.text = feed.food
            reactionDetail.text = feed.reaction
            btnDelete.setOnClickListener { showConfirmationDialog() }
        }
    }
}