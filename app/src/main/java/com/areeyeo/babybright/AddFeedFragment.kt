package com.areeyeo.babybright

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.areeyeo.babybright.data.Feed
import com.areeyeo.babybright.databinding.FragmentAddFeedBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddFeedFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddFeedFragment : Fragment() {


    private val viewModel: FeedViewModel by activityViewModels {
        FeedViewModelFactory(
            (activity?.application as FeedApplication).database.feedDao()
        )
    }

//    private val navigationArgs: FeedDetailFragmentArgs by navArgs()
    lateinit var feed: Feed


    private var _binding: FragmentAddFeedBinding? = null
    private val binding get() = _binding!!

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Hide keyboard.
        val inputMethodManager = requireActivity().getSystemService(INPUT_METHOD_SERVICE) as
                InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
        _binding = null
    }

    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding.feedDatetimeLabel.text.toString(),
            binding.feedFoodLabel.text.toString(),
            binding.feedReactionLabel.text.toString()
        )
    }

    private fun addNewFeed() {
        if (isEntryValid()) {
            viewModel.addNewFeed(
                binding.feedDatetimeLabel.text.toString(),
                binding.feedFoodLabel.text.toString(),
                binding.feedReactionLabel.text.toString()
            )
            val action = AddFeedFragmentDirections.actionAddFeedFragmentToFeedListFragment()
            findNavController().navigate(action)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.saveFeed.setOnClickListener {
            addNewFeed()
        }
    }
}