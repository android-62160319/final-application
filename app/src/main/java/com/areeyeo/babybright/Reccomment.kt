package com.areeyeo.babybright

import androidx.annotation.DrawableRes

data class Affirmation(
    @DrawableRes val imageResourceId: Int
)