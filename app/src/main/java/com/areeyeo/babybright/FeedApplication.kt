package com.areeyeo.babybright

import android.app.Application
import com.areeyeo.babybright.data.FeedRoomDatabase

class FeedApplication : Application(){
    val database: FeedRoomDatabase by lazy { FeedRoomDatabase.getDatabase(this) }
}