package com.areeyeo.babybright.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.areeyeo.babybright.data.Feed
import com.areeyeo.babybright.databinding.FeedListFeedBinding

class FeedListAdapter(private val onItemClicked: (Feed) -> Unit) :
    ListAdapter<Feed, FeedListAdapter.FeedViewHolder>(DiffCallback)  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        return FeedViewHolder(
            FeedListFeedBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }

    class FeedViewHolder(private var binding: FeedListFeedBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(feed: Feed) {
            binding.itemDatetime.text = feed.datetime
            binding.itemFood.text = feed.food
            binding.itemReaction.text = feed.reaction
        }
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Feed>() {
            override fun areItemsTheSame(oldItem: Feed, newItem: Feed): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Feed, newItem: Feed): Boolean {
                return oldItem.datetime == newItem.datetime
            }
        }
    }
}