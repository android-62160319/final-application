package com.areeyeo.babybright.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Feed(
    @PrimaryKey(autoGenerate = true)
    val feed_id: Int = 0,
    @ColumnInfo(name = "datetime")
    val datetime: String,
    @ColumnInfo(name = "food")
    val food: String,
    @ColumnInfo(name = "reaction")
    val reaction: String
)
