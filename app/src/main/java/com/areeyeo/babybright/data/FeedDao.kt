package com.areeyeo.babybright.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface FeedDao {
    @Insert
    suspend fun insert(feed: Feed)
    @Update
    suspend fun update(feed: Feed)
    @Delete
    suspend fun delete(feed: Feed)

    @Query("SELECT * from feed ORDER BY datetime ASC")
    fun getItems(): Flow<List<Feed>>

    @Query("SELECT * from feed WHERE feed_id = :id")
    fun getItem(id: Int): Flow<Feed>
}