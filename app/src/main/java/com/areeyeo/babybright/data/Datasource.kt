package com.areeyeo.babybright.data

import com.areeyeo.babybright.R
import com.areeyeo.babybright.model.Affirmation

/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {

    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
                Affirmation(R.string.affirmation1, R.drawable.img1),
                Affirmation(R.string.affirmation2, R.drawable.img2),
                Affirmation(R.string.affirmation3, R.drawable.img3),
                Affirmation(R.string.affirmation4, R.drawable.img4),
                Affirmation(R.string.affirmation5, R.drawable.img5))
    }
}

